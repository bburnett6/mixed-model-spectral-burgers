#!/bin/bash

METHOD=$1
OUTPUTBASE=./outputs/$METHOD

START=4
END=14
NS=(32 64 128 256)

if [ "$METHOD" = "mixed_sdirk" ]
then
	CORRS=(0)
elif [ "$METHOD" = "mixed_imr" ]
then
	CORRS=(0 1 2)
elif [ "$METHOD" = "sigal_mixed_imr" ]
then
	CORRS=(0 1 2)
else
	CORRS=(0)
fi

for n in ${NS[@]}
do
	for corr in ${CORRS[@]}
	do 
		outputdir=$OUTPUTBASE/n-$n/corr-$corr
		if [ ! -d "$outputdir" ]
		then
			mkdir -p $outputdir
		fi

		#Collect timing/error data
		for texp in $(seq $START $END)
		do
			nt=$((2 ** $texp))
			if [ "$corr" -eq 0 ]
			then
				echo "python spectral/$METHOD.py $n $nt"
				python spectral/$METHOD.py $n $nt > $outputdir/nt-$nt.txt
			else
				echo "python spectral/${METHOD}_corrections.py $n $nt $corr"
				python spectral/${METHOD}_corrections.py $n $nt $corr > $outputdir/nt-$nt.txt
			fi
		done

		#Post process everything into a single file
		#Explaination:
		#1. Grep for the time line
		#2. Sort the grep output "fname:linedata" by splitting on : (-t:) and getting column 1 (-k1) and sort it numerically (-V)
		#3. Print the last column ($NF) of the sorted output which contains the time
		grep "time" $outputdir/* | sort -V -t: -k1 | awk '{print $NF}' > $outputdir/fulltimes.txt
		grep "Error" $outputdir/* | sort -V -t: -k1 | awk '{print $NF}' > $outputdir/fullerrors.txt
	done

	#Paste together a table of all the outputs
	DIR=$OUTPUTBASE/n-$n/corr-
	DATADIRS=( ${CORRS[@]/#/$DIR} ) #creates another array with CORRS appended to DIR: https://stackoverflow.com/questions/27346410/expand-bash-array-into-curly-braces-syntax
	paste ${DATADIRS[@]/%//fullerrors.txt} > $OUTPUTBASE/n-$n/allcorr_errortable.txt
	paste ${DATADIRS[@]/%//fulltimes.txt} > $OUTPUTBASE/n-$n/allcorr_timetable.txt

done