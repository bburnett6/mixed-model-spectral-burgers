
import numpy as np
from scipy.optimize import fsolve
import matplotlib.pyplot as plt
import numpy.fft as fft
import math
import time
import sys
import os 
sys.path.append(os.getcwd())
from initial_conditions import IC

# Initial Conditions
L = IC.L
N = int(sys.argv[1])
dx = L/N
alpha = IC.alpha
ti = IC.ti
tf = IC.tf
nsteps = int(sys.argv[2])
dt = (tf - ti) / nsteps

# Grid
x = np.linspace(0, L-dx, N)
k = fft.fftfreq(N, d=dx/(2.0*np.pi))

un = np.sin(x / (2.0 * np.pi))

def f(v):
	"""
	RHS of spectral form of viscous Burgers equation
	du/dt = -(1/2 u^2)_x + alpha u_xx (regular form)
	dv/dt = -ik FFT(1/2 IFFT(v)^2) - alpha k^2 v
	"""
	return -1j * k * fft.fft(0.5*fft.ifft(v)**2) - alpha * k**2 * v

def y1_rhs(xs, u, dt):
	retf = u + 0.5*dt * fft.ifft(f(fft.fft(xs))).real - xs
	return retf

#############################################################
np.seterr(all='ignore') #Silent all floating point exceptions
#############################################################

start = time.time()
ttotal = ti
for i in range(nsteps):
	if i == nsteps - 1:
		dt = tf - ttotal

	#Fully implicit midpoint rule
	y1 = fsolve(y1_rhs, un, args=(un, dt))

	# update 
	unp1 = un + dt * fft.ifft(f(fft.fft(y1))).real

	ttotal += dt
	un = unp1

elapsed = time.time() - start

fname = f"spectral/ref_sols/ref_sol_n{N}.npy"
with open(fname, 'rb') as f:
	ref_sol = np.load(f)

err_tmp = un - ref_sol
err = math.sqrt(dx * np.dot(err_tmp, err_tmp))
print(f"L2 Error with reference: {err}")
print(f"Elapsed time: {elapsed}")
