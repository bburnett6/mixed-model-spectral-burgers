
import numpy as np
import scipy.optimize as optim
import matplotlib.pyplot as plt
import numpy.fft as fft
import math
import time
import sys
import os 
sys.path.append(os.getcwd())
from initial_conditions import IC

# Initial Conditions
L = IC.L
N = int(sys.argv[1])
dx = L/N
alpha = IC.alpha
ti = IC.ti
tf = IC.tf
nsteps = int(sys.argv[2])
dt = (tf - ti) / nsteps
ncorr = int(sys.argv[3])

# Grid
x = np.linspace(0, L-dx, N)
k = fft.fftfreq(N, d=dx/(2.0*np.pi))

un = np.sin(x / (2.0 * np.pi))


def f(v):
	"""
	RHS of spectral form of viscous Burgers equation
	du/dt = -u u_x + alpha u_xx (regular form)
	dv/dt = -ikv^2 - alpha k^2 v (spectral form, where v = fft(u))
	"""
	return -1j * k * v**2 - alpha * k**2 * v

def j(v):
	"""
	Jacobian of f
	Note: This currently returns a vector!!!
	The jacobian for this spectral system is a diagonal matrix, and 
	because of this, we do not need to do a linear solve. We can 
	simply do an element wise division of the diagonal of J.
	"""
	#return diag(-2j * k * v - alpha * k**2)
	return -2j * k * v - alpha * k**2

#############################################################
np.seterr(all='ignore') #Silent all floating point exceptions
#############################################################
myI = np.ones(N) #not having to call np.ones every iteration saves approx 0.01s for N=256 ie. 8% improvement on performance. Worth.

start = time.time()
ttotal = ti
for i in range(nsteps):
	if i == nsteps - 1:
		dt = tf - ttotal

	un_hat = fft.fft(un)

	# Linearized implicit stage
	xbar = un_hat + 0.5*dt * f(un_hat)
	y1A = myI - 0.5*dt * j(xbar)
	y1b = un_hat + 0.5*dt * (f(xbar) - j(xbar) * xbar)
	#y1_hat = np.linalg.solve(y1A, y1b) #when j is a matrix this is correct
	y1_hat = y1b / y1A #because j is a vector this is correct

	# Corrections
	for k in range(ncorr):
		y1_hat = un_hat + 0.5*dt * f(y1_hat)

	# update 
	unp1_hat = un_hat + dt * f(y1_hat)

	ttotal += dt
	un = fft.ifft(unp1_hat).real

elapsed = time.time() - start

fname = f"spectral/ref_sols/ref_sol_n{N}.npy"
with open(fname, 'rb') as f:
	ref_sol = np.load(f)

err_tmp = un - ref_sol
err = math.sqrt(dx * np.dot(err_tmp, err_tmp))
print(f"L2 Error with reference: {err}")
print(f"Elapsed time: {elapsed}")
