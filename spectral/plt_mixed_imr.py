
import numpy as np
import scipy.optimize as optim
import matplotlib.pyplot as plt
import numpy.fft as fft
import math
import time
import sys
import os 
sys.path.append(os.getcwd())
from initial_conditions import IC
##################################################
#Scipy warns of ignoring the complex part of the jacobian approximation
#Ignore them
import warnings
warnings.filterwarnings('ignore')
###################################################

# Initial Conditions
L = IC.L
N = int(sys.argv[1])
dx = L/N
alpha = IC.alpha
ti = IC.ti
tf = IC.tf
nsteps = int(sys.argv[2])
dt = (tf - ti) / nsteps

# Grid
x = np.linspace(0, L-dx, N)
k = fft.fftfreq(N, d=dx/(2.0*np.pi))

un = np.sin(x / (2.0 * np.pi))

#############################################################
np.seterr(all='ignore') #Silent all floating point exceptions
#############################################################

def f(v):
	"""
	RHS of spectral form of viscous Burgers equation
	du/dt = -(1/2 u^2)_x + alpha u_xx (regular form)
	dv/dt = -ik FFT(1/2 IFFT(v)^2) - alpha k^2 v
	"""
	return -1j * k * fft.fft(0.5*fft.ifft(v)**2) - alpha * k**2 * v

def j(v):
	"""
	Jacobian of f
	The jacobian is a diagonal matrix, so return only that to avoid a linear solve
	"""
	jac = optim.approx_fprime(v, f)
	return np.diagonal(jac) 

plt.ion()
fig, ax = plt.subplots()
ax.plot(x, un)
ax.set_xlim(left=0.0, right=L)
ax.set_ylim(bottom=0.0, top=1.0)
myI = np.ones(N) #not having to call np.ones every iteration saves approx 0.01s for N=256 ie. 8% improvement on performance. Worth.

start = time.time()
ttotal = ti
for i in range(nsteps):
	if i == nsteps - 1:
		dt = tf - ttotal

	un_hat = fft.fft(un)

	# Linearized implicit stage
	xbar = un_hat + 0.5*dt * f(un_hat)
	y1A = myI - 0.5*dt * j(xbar)
	y1b = un_hat + 0.5*dt * (f(xbar) - j(xbar) * xbar)
	#y1_hat = np.linalg.solve(y1A, y1b)
	y1_hat = y1b / y1A

	# update 
	unp1_hat = un_hat + dt * f(y1_hat)

	ttotal += dt
	un = fft.ifft(unp1_hat).real

	ax.cla()
	ax.plot(x, un)
	ax.set_xlim(left=0.0, right=L)
	ax.set_ylim(bottom=0.0, top=1.0)
	plt.title(f"Time: {ttotal:.3f}")
	plt.draw()
	plt.pause(0.001)

elapsed = time.time() - start
print(f"Time to finish: {elapsed}s")

plt.ioff()
plt.show()