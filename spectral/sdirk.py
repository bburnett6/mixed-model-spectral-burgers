
import numpy as np
from scipy.optimize import fsolve
import matplotlib.pyplot as plt
import numpy.fft as fft
import math
import time
import sys
import os 
sys.path.append(os.getcwd())
from initial_conditions import IC

# Initial Conditions
L = IC.L
N = int(sys.argv[1])
dx = L/N
alpha = IC.alpha
ti = IC.ti
tf = IC.tf
nsteps = int(sys.argv[2])
dt = (tf - ti) / nsteps

gamma = (math.sqrt(3.0) + 3.0) / 6.0

# Grid
x = np.linspace(0, L-dx, N)
k = fft.fftfreq(N, d=dx/(2.0*np.pi))

un = np.sin(x / (2.0 * np.pi))

def f(v):
	"""
	RHS of spectral form of viscous Burgers equation
	du/dt = -u u_x + alpha u_xx (regular form)
	dv/dt = -ikv^2 - alpha k^2 v (spectral form, where v = fft(u))
	"""
	return -1j * k * v**2 - alpha * k**2 * v

def y_rhs(xs, u, dt):
	retf = u + gamma*dt * fft.ifft(f(fft.fft(xs))).real - xs
	return retf

#############################################################
np.seterr(all='ignore') #Silent all floating point exceptions
#############################################################

start = time.time()
ttotal = ti
for i in range(nsteps):
	if i == nsteps - 1:
		dt = tf - ttotal

	# Implicit stage 1
	y1 = fsolve(y_rhs, un, args=(un, dt))

	# Implicit stage 2
	tmp = un + (1.0 - 2.0*gamma)*dt * fft.ifft(f(fft.fft(y1))).real
	y2 = fsolve(y_rhs, un, args=(tmp, dt))

	# update 
	unp1 = un + 0.5*dt * fft.ifft(f(fft.fft(y1))).real + 0.5*dt * fft.ifft(f(fft.fft(y2))).real

	ttotal += dt
	un = unp1

elapsed = time.time() - start

fname = f"spectral/ref_sols/ref_sol_n{N}.npy"
with open(fname, 'rb') as f:
	ref_sol = np.load(f)

err_tmp = un - ref_sol
err = math.sqrt(dx * np.dot(err_tmp, err_tmp))
print(f"L2 Error with reference: {err}")
print(f"Elapsed time: {elapsed}")
