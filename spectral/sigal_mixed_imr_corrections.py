
import numpy as np
import scipy.optimize as optim
import matplotlib.pyplot as plt
import numpy.fft as fft
import math
import time
import sys
import os 
sys.path.append(os.getcwd())
from initial_conditions import IC
##################################################
#Scipy warns of ignoring the complex part of the jacobian approximation
#Ignore them
import warnings
warnings.filterwarnings('ignore')
###################################################

# Initial Conditions
L = IC.L
N = int(sys.argv[1])
dx = L/N
alpha = IC.alpha
ti = IC.ti
tf = IC.tf
nsteps = int(sys.argv[2])
dt = (tf - ti) / nsteps
ncorr = int(sys.argv[3])

# Grid
x = np.linspace(0, L-dx, N)
k = fft.fftfreq(N, d=dx/(2.0*np.pi))

un = np.sin(x / (2.0 * np.pi))

def f(v):
	"""
	RHS of spectral form of viscous Burgers equation
	du/dt = -(1/2 u^2)_x + alpha u_xx (regular form)
	dv/dt = -ik FFT(1/2 IFFT(v)^2) - alpha k^2 v
	"""
	return -1j * k * fft.fft(0.5*fft.ifft(v)**2) - alpha * k**2 * v

#############################################################
np.seterr(all='ignore') #Silent all floating point exceptions
#############################################################
myI = np.ones(N) #not having to call np.ones every iteration saves approx 0.01s for N=256 ie. 8% improvement on performance. Worth.

start = time.time()
ttotal = ti
for i in range(nsteps):
	if i == nsteps - 1:
		dt = tf - ttotal

	un_hat = fft.fft(un)

	# Linearized implicit stage
	ubar = un #+ 0.5*dt * f(un)
	y1A = myI + 0.5*dt * (1j * k * ubar + alpha * k**2)
	y1b = un_hat
	y1_hat = y1b / y1A 
	y1 = fft.ifft(y1_hat).real

	for p in range(ncorr):
		y1 = un + 0.5*dt * fft.ifft( -1j * k * fft.fft(0.5 * y1**2) - k**2 *y1_hat).real

	#for p in range(ncorr):
	#	y1_hat = un_hat + 0.5*dt * f(y1_hat)

	# update 
	unp1 = un + dt * fft.ifft( -1j * k * fft.fft(0.5 * y1**2) - k**2 *y1_hat).real
	#unp1_hat = un_hat + dt * f(y1_hat)

	ttotal += dt
	un = unp1
	#un = fft.ifft(unp1_hat).real

elapsed = time.time() - start

fname = f"spectral/ref_sols/ref_sol_n{N}.npy"
with open(fname, 'rb') as f:
	ref_sol = np.load(f)

err_tmp = un - ref_sol
err = math.sqrt(dx * np.dot(err_tmp, err_tmp))
print(f"L2 Error with reference: {err}")
print(f"Elapsed time: {elapsed}")
