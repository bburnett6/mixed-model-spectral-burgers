
import numpy as np
import scipy.optimize as optim
import matplotlib.pyplot as plt
import numpy.fft as fft
import math
import time
import sys
import os 
sys.path.append(os.getcwd())
from initial_conditions import IC
##################################################
#Scipy warns of ignoring the complex part of the jacobian approximation
#Ignore them
import warnings
warnings.filterwarnings('ignore')
###################################################

# Initial Conditions
L = IC.L
N = int(sys.argv[1])
dx = L/N
alpha = IC.alpha
ti = IC.ti
tf = IC.tf
nsteps = int(sys.argv[2])
dt = (tf - ti) / nsteps

# Grid
x = np.linspace(0, L-dx, N)
k = fft.fftfreq(N, d=dx/(2.0*np.pi))

un = np.sin(x / (2.0 * np.pi))

#############################################################
np.seterr(all='ignore') #Silent all floating point exceptions
#############################################################
myI = np.ones(N) #not having to call np.ones every iteration saves approx 0.01s for N=256 ie. 8% improvement on performance. Worth.

plt.ion()
fig, ax = plt.subplots()
ax.plot(x, un)
ax.set_xlim(left=0.0, right=L)
ax.set_ylim(bottom=0.0, top=1.0)

start = time.time()
ttotal = ti
for i in range(nsteps):
	if i == nsteps - 1:
		dt = tf - ttotal

	un_hat = fft.fft(un)

	# Linearized implicit stage
	ubar = un #+ 0.5*dt * f(un)
	y1A = myI + 0.5*dt * (1j * k * ubar + alpha * k**2)
	y1b = un_hat
	y1_hat = y1b / y1A 
	y1 = fft.ifft(y1_hat).real

	for p in range(2):
		y1 = un + 0.5*dt * fft.ifft( -1j * k * fft.fft(0.5 * y1**2) - k**2 *y1_hat).real

	# update 
	unp1 = un + dt * fft.ifft( -1j * k * fft.fft(0.5 * y1**2) - k**2 *y1_hat).real

	ttotal += dt
	un = unp1

	ax.cla()
	ax.plot(x, un)
	ax.set_xlim(left=0.0, right=L)
	ax.set_ylim(bottom=0.0, top=1.0)
	plt.title(f"Time: {ttotal:.3f}")
	plt.draw()
	plt.pause(0.001)

elapsed = time.time() - start
print(f"Time to finish: {elapsed}s")

plt.ioff()
plt.show()