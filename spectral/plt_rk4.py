
import numpy as np
import scipy.optimize as optim
import matplotlib.pyplot as plt
import numpy.fft as fft
import math
import time
import sys
import os 
sys.path.append(os.getcwd())
from initial_conditions import IC

# Initial Conditions
L = IC.L
N = int(sys.argv[1])
dx = L/N
alpha = IC.alpha
ti = IC.ti
tf = IC.tf
nsteps = int(sys.argv[2])
dt = (tf - ti) / nsteps

# Grid
x = np.linspace(0, L-dx, N)
k = fft.fftfreq(N, d=dx/(2.0*np.pi))

un = np.sin(x / (2.0 * np.pi))

def f(v):
	"""
	RHS of spectral form of viscous Burgers equation
	du/dt = -(1/2 u^2)_x + alpha u_xx (regular form)
	dv/dt = -ik FFT(1/2 IFFT(v)^2) - alpha k^2 v
	"""
	return -1j * k * fft.fft(0.5*fft.ifft(v)**2) - alpha * k**2 * v

plt.ion()
fig, ax = plt.subplots()
ax.plot(x, un)
ax.set_xlim(left=0.0, right=L)
ax.set_ylim(bottom=0.0, top=1.0)

start = time.time()
ttotal = ti
for i in range(nsteps):
	if i == nsteps - 1:
		dt = tf - ttotal

	un_hat = fft.fft(un)

	k1 = f(un_hat)

	tmp = un_hat + 0.5*dt * k1
	k2 = f(tmp)

	tmp = un_hat + 0.5*dt * k2
	k3 = f(tmp)

	tmp = un_hat + dt * k3
	k4 = f(tmp)

	unp1_hat = un_hat + dt/6.0 * (k1 + 2.0*k2 + 2.0*k3 + k4)

	ttotal += dt
	un = fft.ifft(unp1_hat).real

	ax.cla()
	ax.plot(x, un)
	ax.set_xlim(left=0.0, right=L)
	ax.set_ylim(bottom=0.0, top=1.0)
	plt.title(f"Time: {ttotal:.3f}")
	plt.draw()
	plt.pause(0.001)

elapsed = time.time() - start
print(f"Time to finish: {elapsed}")

plt.ioff()
plt.show()