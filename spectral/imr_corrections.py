
import numpy as np
import scipy.optimize as optim
import matplotlib.pyplot as plt
import numpy.fft as fft
import math
import time
import sys
import os 
sys.path.append(os.getcwd())
from initial_conditions import IC

# Initial Conditions
L = IC.L
N = int(sys.argv[1])
dx = L/N
alpha = IC.alpha
ti = IC.ti
tf = IC.tf
nsteps = int(sys.argv[2])
dt = (tf - ti) / nsteps
ncorr = int(sys.argv[3])

# Grid
x = np.linspace(0, L-dx, N)
k = fft.fftfreq(N, d=dx/(2.0*np.pi))

un = np.sin(x / (2.0 * np.pi))
unp1_hat = np.zeros(N)

def f(v):
	"""
	RHS of spectral form of viscous Burgers equation
	du/dt = -u u_x + alpha u_xx (regular form)
	dv/dt = -ikv^2 - alpha k^2 v (spectral form, where v = fft(u))
	"""
	return -1j * k * v**2 - alpha * k**2 * v

def j(v):
	"""
	Jacobian of f
	"""
	return np.diag(-2j * k * v - alpha * k**2)

plt.ion()
fig, ax = plt.subplots()
ax.plot(x, un)
ax.set_xlim(left=0.0, right=L)
ax.set_ylim(bottom=0.0, top=1.0)

plt_every = 10

start = time.time()
ttotal = ti
for i in range(nsteps):
	if i == nsteps - 1:
		dt = tf - ttotal

	un_hat = fft.fft(un)

	# Linearized implicit stage
	xbar = un_hat + 0.5*dt * f(un_hat)
	y1A = np.identity(N) - 0.5*dt * j(xbar)
	y1b = un_hat + 0.5*dt * (f(xbar) - j(xbar).dot(xbar))
	y1_hat = np.linalg.solve(y1A, y1b)

	# Corrections
	for k in range(ncorr):
		y1_hat = un_hat + 0.5*dt * f(y1_hat)

	# update 
	print(unp1_hat)
	print("-----------")
	unp1_hat = un_hat + dt * f(y1_hat)
	print(unp1_hat)
	print("============")

	ttotal += dt
	un = fft.ifft(unp1_hat).real

	if i % plt_every == 0:
		#print(un)
		ax.cla()
		ax.plot(x, un)
		ax.set_xlim(left=0.0, right=L)
		ax.set_ylim(bottom=0.0, top=1.0)
		plt.title(f"Time: {ttotal:.3f}")
		plt.draw()
		plt.pause(0.01)

elapsed = time.time() - start
print(f"Time to finish: {elapsed}")

plt.ioff()
plt.show()