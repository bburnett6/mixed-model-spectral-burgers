
import numpy as np
import numpy.fft as fft
import matplotlib.pyplot as plt

N = 64
L = 20
points = np.arange(0, N, 1)
dx = L / N

#initialize the gridpoints
xs = points * np.pi * 2.0 / N
xs = L * xs / (2.0*np.pi)
print(f"{dx:.8f}")
print(f"{(xs[1] - xs[0]):.8f}")

#initialize matrices
D = np.zeros((N, N)) #Differentiation matrix
D2 = np.zeros((N, N)) #2nd order differentiation matrix
F = np.zeros((N, N), dtype=np.complex_) #Fourier transform matrix (FTM)
k = fft.fftfreq(N, d=dx/(2.0*np.pi)) 
K = np.diag(k)

# Compute the differentiation matrix using fourier transforms of the Identity
Istore = np.eye(N)
for i in range(N):
	F[i, :] = fft.fft(Istore[i, :])
FI = np.linalg.inv(F)
D = FI @ (1j * K @ F)
D2 = FI @ (-K**2 @ F)

#Compute the derivatives
u = np.sin(xs * 2.0*np.pi / L)
du_exact = 2.0*np.pi / L * np.cos(xs * 2.0*np.pi / L)
ddu_exact = -(2.0*np.pi / L)**2 * np.sin(xs * 2.0*np.pi / L)
du = D.real.dot(u)
ddu = D2.real.dot(u)

#plot the function and its derivative
plt.plot(u, label="sin")
plt.plot(du_exact, label="cos")
plt.plot(du, ls='-.', label="derivative")
plt.plot(ddu_exact, label="-sin")
plt.plot(ddu, ls='-.', label="2nd derivative")
plt.legend()
plt.show()

#plot fourier solutions comparing the ftm to the fft
#uf = F.dot(u)
#uhat = fft.fft(u)
#plt.plot(uhat, label="'exact'")
#plt.plot(uf, ls='-.', label="dot")