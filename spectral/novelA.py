
import numpy as np
import scipy.optimize as optim
import matplotlib.pyplot as plt
import numpy.fft as fft
import math
import time
import sys
import os 
sys.path.append(os.getcwd())
from initial_conditions import IC

# Initial Conditions
L = IC.L
N = int(sys.argv[1])
dx = L/N
alpha = IC.alpha
ti = IC.ti
tf = IC.tf
nsteps = int(sys.argv[2])
dt = (tf - ti) / nsteps

ar = np.zeros((4, 4))
ar[1, 0] = 0.211324865405187
ar[2, 0] = 0.709495523817170
ar[2, 1] = -0.865314250619423
ar[3, 0] = 0.705123240545107
ar[3, 1] = 0.943370088535775
ar[3, 2] = -0.859818194486069

ae = np.zeros((4, 4))
ae[0, 0] = 0.788675134594813
ae[2, 0] = 0.051944240459852
ae[2, 2] = 0.788675134594813

b = np.array([0, 0.5, 0, 0.5])

# Grid
x = np.linspace(0, L-dx, N)
k = fft.fftfreq(N, d=dx/(2.0*np.pi))

un = np.sin(x / (2.0 * np.pi))


def f(v):
	"""
	RHS of spectral form of viscous Burgers equation
	du/dt = -u u_x + alpha u_xx (regular form)
	dv/dt = -ikv^2 - alpha k^2 v (spectral form, where v = fft(u))
	"""
	return -1j * k * v**2 - alpha * k**2 * v

def j(v):
	"""
	Jacobian of f
	Note: This currently returns a vector!!!
	The jacobian for this spectral system is a diagonal matrix, and 
	because of this, we do not need to do a linear solve. We can 
	simply do an element wise division of the diagonal of J.
	"""
	#return diag(-2j * k * v - alpha * k**2)
	return -2j * k * v - alpha * k**2

#############################################################
np.seterr(all='ignore') #Silent all floating point exceptions
#############################################################
myI = np.ones(N) #not having to call np.ones every iteration saves approx 0.01s for N=256 ie. 8% improvement on performance. Worth.

start = time.time()
ttotal = ti
for i in range(nsteps):
	if i == nsteps - 1:
		dt = tf - ttotal

	un_hat = fft.fft(un)

	# Stage 1 implicit
	xbar = un_hat + ae[0, 0]*dt * f(un_hat)
	y1A = myI - ae[0, 0]*dt * j(xbar)
	y1b = un_hat + ae[0, 0]*dt * (f(xbar) - j(xbar) * xbar)
	y1_hat = y1b / y1A #because j is a vector this is correct

	# Stage 2 explicit
	y2_hat = un_hat + ar[1, 0]*dt * f(y1_hat)

	# Stage 3 implicit
	xbar = un_hat 
	xbar += ar[2, 0]*dt * f(y1_hat) 
	xbar += ar[2, 1]*dt * f(y2_hat) 
	xbar += ae[2, 0]*dt * f(y1_hat) 
	xbar += ae[2, 2]*dt * f(un_hat)

	y3A = myI - ae[2, 2]*dt * j(xbar)

	y3b = un_hat 
	y3b += ar[2, 0]*dt * f(y1_hat) 
	y3b += ar[2, 1]*dt * f(y2_hat) 
	y3b += ae[2, 0]*dt * f(y1_hat) 
	y3b += ae[2, 2]*dt * (f(xbar) - j(xbar) * xbar)
	y3_hat = y3b / y3A

	# Stage 4 explicit
	y4_hat = un_hat 
	y4_hat += ar[3, 0]*dt * f(y1_hat) 
	y4_hat += ar[3, 1]*dt * f(y2_hat) 
	y4_hat += ar[3, 2]*dt * f(y3_hat)
	y4_hat += ae[3, 0]*dt * f(y1_hat) 
	y4_hat += ae[3, 1]*dt * f(y2_hat) 
	y4_hat += ae[3, 2]*dt * f(y3_hat)

	# update 
	unp1_hat = un_hat + b[1]*dt * f(y2_hat) + b[3]*dt * f(y4_hat)

	ttotal += dt
	un = fft.ifft(unp1_hat).real

elapsed = time.time() - start

fname = f"spectral/ref_sols/ref_sol_n{N}.npy"
with open(fname, 'rb') as f:
	ref_sol = np.load(f)

err_tmp = un - ref_sol
err = math.sqrt(dx * np.dot(err_tmp, err_tmp))
print(f"L2 Error with reference: {err}")
print(f"Elapsed time: {elapsed}")
